import java.util.Scanner;

public class Gravitacija {
    public static void main (String args[]) {
       
        Scanner sc = new Scanner(System.in);
        double h = sc.nextInt();
		
        double a = pospesek(h);
        
        izpis(h, a);
    }
    
    public static double pospesek(double h){
	
		double C = 6.674 * Math.pow(10.0,-11.0);
        double M = 5.972 * Math.pow(10.0,24.0);
        double r = 6.371 * Math.pow(10.0,6.0);
		
		double aa = C * M / Math.pow(r + h,2);
        
        return aa;  
    }
    
    static void izpis (double visina, double pospesek) {
        System.out.println("Pri nadmorski visini "+visina+" je gravitacijski pospesek "+pospesek+".");
    }
}